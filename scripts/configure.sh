#!/bin/bash

# root user password for MySQL (Feel free to change if you desire)
PASSWORD='password'

# Download and install newest package updates
apt-get update && apt-get upgrade -y

# Set MySQL root password for silent install
debconf-set-selections <<< "mysql-server mysql-server/root_password password $PASSWORD"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $PASSWORD"

# Install LAMP stack
apt install -y apache2 mysql-server php7.0

##### APACHE CONFIGURATION #####

# Apache conf for permissions to "mount"
cat << EOF > /etc/apache2/conf-available/website_mnt.conf
<Directory /mnt/website/>
        Options Indexes FollowSymLinks
        AllowOverride None
        Require all granted
</Directory>
EOF

cat << EOF > /etc/apache2/sites-available/999-website.conf
<VirtualHost *:80>
	ServerAdmin webmaster@localhost
	DocumentRoot /mnt/website
	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOF

a2dissite 000-default.conf
a2enconf website_mnt
a2ensite 999-website.conf

systemctl reload apache2